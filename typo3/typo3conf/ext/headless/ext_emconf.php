<?php

/***************************************************************
 * Extension Manager/Repository config file for ext "headless".
 *
 * Auto generated 12-01-2021 13:53
 *
 * Manual updates:
 * Only the data in the array - everything else is removed by next
 * writing. "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = array (
  'title' => 'Headless',
  'description' => 'This extension provides way to output content from TYPO3 in JSON format.',
  'state' => 'stable',
  'author' => 'Łukasz Uznański',
  'author_email' => 'extensions@macopedia.pl',
  'category' => 'fe',
  'version' => '2.3.0',
  'constraints' => 
  array (
    'depends' => 
    array (
      'typo3' => '9.5.0-10.4.99',
      'frontend' => '9.5.0-10.4.99',
    ),
    'conflicts' => 
    array (
    ),
    'suggests' => 
    array (
    ),
  ),
  'uploadfolder' => true,
  'clearcacheonload' => true,
  'author_company' => NULL,
);

