<?php
namespace Wem\WemHeadless\Utility;


class FlexformUtility {

    /**
     * return colors from typoscript in select box
     *
     * @param $fConfig
     * @param $fObj
     */
    public function getColorSelect(&$fConfig, $fObj) {

        $colors = array(
            ['name' => 'default', 'value' => ''],
            ['name' => 'white', 'value' => 'white'],
            ['name' => 'black', 'value' => 'black'],
            ['name' => 'magenta', 'value' => 'magenta'],
            ['name' => 'blue', 'value' => 'blue'],
            ['name' => 'yellow', 'value' => 'yellow'],
        );

        foreach($colors as $key => $color) {
            array_push(
                $fConfig['items'],
                array(
                    $color['name'],
                    $color['value'],
                )
            );
        }
    }

    public function getSubheaderLayoutSelect(&$fConfig, $fObj) {
        $colors = array(
            ['name' => '', 'value' => ''],
            ['name' => 'h1', 'value' => '1'],
            ['name' => 'h2', 'value' => '2'],
            ['name' => 'h3', 'value' => '3'],
            ['name' => 'h4', 'value' => '4'],
            ['name' => 'h5', 'value' => '5'],
            ['name' => 'h6', 'value' => '6'],
        );

        foreach($colors as $key => $color) {
            array_push(
                $fConfig['items'],
                array(
                    $color['name'],
                    $color['value'],
                )
            );
        }
    }
    /**
     * Gridelements functions
     * @param $fConfig
     * @param $fObj
     */
    public function getColumnXlWidths(&$fConfig, $fObj) {
        $this->getColumnWidths($fConfig, 'xl');
    }

    public function getColumnXlOffsets(&$fConfig, $fObj) {
        $this->getColumnOffsets($fConfig, 'xl');
    }
    public function getColumnLgWidths(&$fConfig, $fObj) {
        $this->getColumnWidths($fConfig, 'lg');
    }

    public function getColumnLgOffsets(&$fConfig, $fObj) {
        $this->getColumnOffsets($fConfig, 'lg');
    }

    public function getColumnMdWidths(&$fConfig, $fObj) {
        $this->getColumnWidths($fConfig, 'md');
    }

    public function getColumnMdOffsets(&$fConfig, $fObj) {
        $this->getColumnOffsets($fConfig, 'md');
    }

    public function getColumnSmWidths(&$fConfig, $fObj) {
        $this->getColumnWidths($fConfig, 'sm');
    }

    public function getColumnSmOffsets(&$fConfig, $fObj) {
        $this->getColumnOffsets($fConfig, 'sm');
    }

    public function getColumnXsWidths(&$fConfig, $fObj) {
        $this->getColumnWidths($fConfig);
    }

    public function getColumnXsOffsets(&$fConfig, $fObj) {
        $this->getColumnOffsets($fConfig);
    }

    /**
     * @param $fConfig
     * @param null $size
     */
    private function getColumnWidths(&$fConfig, $size = null) {
        $tag = '';
        if ($size !== null) {
            $tag = $size . '-';
        }

        array_push(
            $fConfig['items'],
            array(
                '',
                '',
            )
        );

        for($i=1; $i<=12;$i++) {
            array_push(
                $fConfig['items'],
                array(
                    $i . '/' . 12,
                    'col-' . $tag . $i,
                )
            );
        }
    }

    private function getColumnOffsets(&$fConfig, $size = null) {
        $tag = '';
        if ($size !== null) {
            $tag = $size . '-';
        }
        array_push(
            $fConfig['items'],
            array(
                '',
                '',
            )
        );
        for($i=1; $i<=12;$i++) {
            array_push(
                $fConfig['items'],
                array(
                    $i . '/' . 12,
                    'offset-' . $tag . $i,
                )
            );
        }
    }

    public function getSizes (&$fConfig) {
        $sizes = array(
            'xs' => 'Extra Small',
            'sm' => 'Small',
            'md' => 'Medium',
            'lg' => 'Large',
            'xl' => 'Extra large',
        );
        foreach ($sizes as $key => $val) {
            array_push(
                $fConfig['items'],
                array(
                    $val,
                    $key,
                )
            );
        }
    }
}
