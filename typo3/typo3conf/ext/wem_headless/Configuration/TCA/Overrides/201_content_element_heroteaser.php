<?php

/*
 * This file is part of the package wem/wem-theme.
 *
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

defined('TYPO3_MODE') || die();

/***************
 * Add Content Element
 */
if (!is_array($GLOBALS['TCA']['tt_content']['types']['heroteaser'])) {
    $GLOBALS['TCA']['tt_content']['types']['heroteaser'] = [];
}

/***************
 * Add content element PageTSConfig
 */
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::registerPageTSConfigFile(
    $extensionKey,
    'Configuration/TsConfig/Page/ContentElement/Element/Heroteaser.tsconfig',
    'Heroteaser'
);

/***************
 * Add content element to selector list
 */
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTcaSelectItem(
    'tt_content',
    'CType',
    [
        'LLL:EXT:wem_headless/Resources/Private/Language/Backend.xlf:content_element.heroteaser',
        'heroteaser',
        'mimetypes-x-content-multimedia'
    ],
    'list',
    'after'
);

/***************
 * Assign Icon
 */
$GLOBALS['TCA']['tt_content']['ctrl']['typeicon_classes']['heroteaser'] = 'mimetypes-x-content-multimedia';

/***************
 * Configure element type
 */
$GLOBALS['TCA']['tt_content']['palettes']['heroteaser__default'] = [
    'showitem' => '
        header, header_layout, header_color, --linebreak--,
        subheader, subheader_layout, subheader_color, --linebreak--,
        bodytext, --linebreak--,
        image, --linebreak--,
    '
];
$GLOBALS['TCA']['tt_content']['types']['heroteaser'] = array_replace_recursive(
    $GLOBALS['TCA']['tt_content']['types']['heroteaser'],
    [
        'showitem' => '
            --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
                --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.general;general,
                tx_wemheadless_type,
                --palette--;;heroteaser__default,
            --div--;LLL:EXT:heroteaser/Resources/Private/Language/Backend.xlf:slider.settings,
            --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.appearance,
                --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.frames;frames,
                --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.appearanceLinks;appearanceLinks,
            --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,
                --palette--;;language,
            --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:access,
                --palette--;;hidden,
                --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.access;access,
            --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories,
                categories,
            --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:notes,
                rowDescription,
            --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended,
        ',
        'columnsOverrides' => [
            'bodytext' => [
                'displayCond' => 'FIELD:tx_wemheadless_type:!=:1',
                'config' => [
                    'enableRichtext' => true,
                    'richtextConfiguration' => 'default'
                ]
            ],
            'header' => [
                'displayCond' => 'FIELD:tx_wemheadless_type:!=:1'
            ],
            'image' => [
                'displayCond' => 'FIELD:tx_wemheadless_type:!=:1'
            ],
            'media' => [
                'displayCond' => 'FIELD:tx_wemheadless_type:=:1',
                'config' => [
                    'max' => 1,
                    'eval' => 'required',
                ]
            ],
        ]
    ]
);


/***************
 * Register fields
 */
$GLOBALS['TCA']['tt_content']['columns'] = array_replace_recursive(
    $GLOBALS['TCA']['tt_content']['columns'],
    [
        'tx_wemheadless_type' => [
            'label' => 'Type',
            'onChange' => 'reload',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => [
                    [
                        'Default',
                        '0'
                    ],
                    [
                        'Startseite',
                        '1'
                    ],
                ],
                'default' => 0,
            ],
        ],
    ]
);
