<?php

/*
 * This file is part of the package wem/wem-theme.
 *
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

defined('TYPO3_MODE') || die();

$GLOBALS['TCA']['tt_content']['columns']['bodytext']['l10n_mode'] = '';
$GLOBALS['TCA']['tt_content']['columns']['header']['l10n_mode'] = '';
$GLOBALS['TCA']['pages_language_overlay']['columns']['title']['l10n_mode'] = 'exclude';


\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('tt_content', [
    'subheader_layout' => [
        'exclude' => 1,
        'label' => 'Subheader Layout',
        'config' => [
            'type' => 'select',
            'renderType' => 'selectSingle',
            'itemsProcFunc' => 'Wem\\WemHeadless\\Utility\\FlexformUtility->getSubheaderLayoutSelect',
        ]
    ],
    'subheader_color' => [
        'exclude' => 1,
        'label' => 'SubHeader color',
        'config' => [
            'type' => 'select',
            'renderType' => 'selectSingle',
            'itemsProcFunc' => 'Wem\\WemHeadless\\Utility\\FlexformUtility->getColorSelect',
        ]
    ],
    'header_color' => [
        'exclude' => 1,
        'label' => 'Header color',
        'config' => [
            'type' => 'select',
            'renderType' => 'selectSingle',
            'itemsProcFunc' => 'Wem\\WemHeadless\\Utility\\FlexformUtility->getColorSelect',
        ]
    ],
    'header_class' => [
        'exclude' => 1,
        'label' => 'Header class',
        'config' => [
            'type' => 'select',
            'renderType' => 'selectSingle',
            'itemsProcFunc' => 'Wem\\WemHeadless\\Utility\\FlexformUtility->getSubheaderLayoutSelect',
        ]
    ],
    'header_intend' => [
        'exclude' => 1,
        'label' => 'intend',
        'config' => [
            'type' => 'check',
            'items' => [
                '1' => [
                    '0' => 'intend'
                ]
            ]
        ]
    ],

]);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addFieldsToPalette('tt_content', 'headers', 'subheader_layout', 'after:subheader');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addFieldsToPalette('tt_content', 'headers', 'subheader_color', 'after:subheader_layout');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addFieldsToPalette('tt_content', 'headers', 'header_color', 'after:header');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addFieldsToPalette('tt_content', 'headers', 'header_class', 'after:header_layout');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addFieldsToPalette('tt_content', 'headers', 'header_intend', 'after:header_class');
