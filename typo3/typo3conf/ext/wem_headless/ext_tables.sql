CREATE TABLE tt_content (
    tx_wemheadless_type int(11) unsigned DEFAULT '0',
    subheader_layout varchar(25) DEFAULT 'h2' NOT NULL,
    header_color varchar(255) DEFAULT '' NOT NULL,
    header_class varchar(255) DEFAULT '' NOT NULL,
    header_intend int(11) unsigned DEFAULT '0',
    subheader_color varchar(255) DEFAULT '' NOT NULL,
    tx_wemheadless_contact int(11) unsigned DEFAULT '0'
);


#
# Table structure for table 'tx_wemheadless_contact'
#
CREATE TABLE tx_wemheadless_contact
(
    uid              int(11) unsigned              NOT NULL auto_increment,
    pid              int(11)           DEFAULT '0' NOT NULL,

    name             varchar(255)      DEFAULT ''  NOT NULL,
    position         varchar(255)      DEFAULT ''  NOT NULL,
    image            int(11) unsigned  DEFAULT '0',
    email             varchar(255)     DEFAULT 'info@schliesske.de'  NOT NULL,
    phone      varchar(255)     DEFAULT ''  NOT NULL,
    facebook      varchar(255)     DEFAULT ''  NOT NULL,
    twitter      varchar(255)     DEFAULT ''  NOT NULL,
    xing      varchar(255)     DEFAULT ''  NOT NULL,
    pinterest      varchar(255)     DEFAULT ''  NOT NULL,

    tstamp           int(11) unsigned  DEFAULT '0' NOT NULL,
    crdate           int(11) unsigned  DEFAULT '0' NOT NULL,
    cruser_id        int(11) unsigned  DEFAULT '0' NOT NULL,
    deleted          smallint unsigned DEFAULT '0' NOT NULL,
    hidden           smallint unsigned DEFAULT '0' NOT NULL,
    starttime        int(11) unsigned  DEFAULT '0' NOT NULL,
    endtime          int(11) unsigned  DEFAULT '0' NOT NULL,
    sorting          int(11)           DEFAULT '0' NOT NULL,

    sys_language_uid int(11)           DEFAULT '0' NOT NULL,
    l10n_parent      int(11) unsigned  DEFAULT '0' NOT NULL,
    l10n_diffsource  mediumblob                    NULL,

    t3ver_oid        int(11) unsigned  DEFAULT '0' NOT NULL,
    t3ver_id         int(11) unsigned  DEFAULT '0' NOT NULL,
    t3ver_wsid       int(11) unsigned  DEFAULT '0' NOT NULL,
    t3ver_label      varchar(255)      DEFAULT ''  NOT NULL,
    t3ver_state      smallint          DEFAULT '0' NOT NULL,
    t3ver_stage      int(11)           DEFAULT '0' NOT NULL,
    t3ver_count      int(11) unsigned  DEFAULT '0' NOT NULL,
    t3ver_tstamp     int(11) unsigned  DEFAULT '0' NOT NULL,
    t3ver_move_id    int(11) unsigned  DEFAULT '0' NOT NULL,
    t3_origuid       int(11) unsigned  DEFAULT '0' NOT NULL,

    PRIMARY KEY (uid),
    KEY parent (pid),
    KEY t3ver_oid (t3ver_oid, t3ver_wsid),
    KEY language (l10n_parent, sys_language_uid)
);
