<?php

/*
 * This file is part of the package wem/wem-theme.
 *
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

defined('TYPO3_MODE') || die();

/***************
 * Define TypoScript as content rendering template
 */
$GLOBALS['TYPO3_CONF_VARS']['FE']['contentRenderingTemplates'][] = 'wemtheme/Configuration/TypoScript/';
$GLOBALS['TYPO3_CONF_VARS']['FE']['contentRenderingTemplates'][] = 'wemtheme/Configuration/TypoScript/ContentElement/';

/***************
 * PageTS
 */

// Add Content Elements
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig('<INCLUDE_TYPOSCRIPT: source="FILE:EXT:wem_headless/Configuration/TsConfig/Page/ContentElement/All.tsconfig">');


// Add BackendLayouts for the BackendLayout DataProvider
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig('<INCLUDE_TYPOSCRIPT: source="FILE:EXT:wem_headless/Configuration/TsConfig/Page/Mod/WebLayout/BackendLayouts.tsconfig">');


// RTE
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig('<INCLUDE_TYPOSCRIPT: source="FILE:EXT:wem_headless/Configuration/TsConfig/Page/RTE.tsconfig">');


// TCEMAIN
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig('<INCLUDE_TYPOSCRIPT: source="FILE:EXT:wem_headless/Configuration/TsConfig/Page/TCEMAIN.tsconfig">');


// TCEFORM
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig('<INCLUDE_TYPOSCRIPT: source="FILE:EXT:wem_headless/Configuration/TsConfig/Page/TCEFORM.tsconfig">');


// Gridelements
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig('<INCLUDE_TYPOSCRIPT: source="FILE:EXT:wem_headless/Configuration/TsConfig/GridElements.tsconfig">');

/***************
 * Register custom EXT:form configuration
 */
if (\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::isLoaded('form')) {
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTypoScriptSetup(trim('
        module.tx_form {
            settings {
                yamlConfigurations {
                    110 = EXT:wem_headless/Configuration/Form/Setup.yaml
                }
            }
        }
        plugin.tx_form {
            settings {
                yamlConfigurations {
                    110 = EXT:wem_headless/Configuration/Form/Setup.yaml
                }
            }
        }
    '));
}

if (TYPO3_MODE === 'BE') {
    $signalSlotDispatcher = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Extbase\SignalSlot\Dispatcher::class);

    /**
     * Provide example webserver configuration after extension is installed.
     */
    $signalSlotDispatcher->connect(
        \TYPO3\CMS\Extensionmanager\Service\ExtensionManagementService::class,
        'hasInstalledExtensions',
        \Wem\WemTheme\Service\InstallService::class,
        'generateApacheHtaccess'
    );

    /**
     * Add backend styling
     */
    $signalSlotDispatcher->connect(
        \TYPO3\CMS\Extensionmanager\Service\ExtensionManagementService::class,
        'hasInstalledExtensions',
        \Wem\WemTheme\Service\BrandingService::class,
        'setBackendStyling'
    );
}

/***************
 * Add default RTE configuration for wem theme
 */
$GLOBALS['TYPO3_CONF_VARS']['RTE']['Presets']['bootstrap'] = 'EXT:wem_headless/Configuration/RTE/Default.yaml';

/***************
 * Register "wem" as global fluid namespace
 */
$GLOBALS['TYPO3_CONF_VARS']['SYS']['fluid']['namespaces']['wem'][] = 'Wem\\WemTheme\\ViewHelpers';

/***************
 * Register Icons
 */
$iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);
$iconRegistry->registerIcon(
    'systeminformation-wemtheme',
    \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
    ['source' => 'EXT:wem_headless/Resources/Public/Icons/SystemInformation/wemtheme.svg']
);
$icons = [
];
foreach ($icons as $icon) {
    $iconRegistry->registerIcon(
        'content-wemtheme-' . $icon,
        \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
        ['source' => 'EXT:wem_headless/Resources/Public/Icons/ContentElements/' . $icon . '.svg']
    );
}

/***************
 * tx_news extends
 */
    /*
$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS'][\TYPO3\CMS\Core\Configuration\FlexForm\FlexFormTools::class]['flexParsing'][]
    = \Wem\WemTheme\Hooks\News\FlexFormHook::class;
*/
