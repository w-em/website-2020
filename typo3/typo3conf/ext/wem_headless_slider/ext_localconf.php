<?php
defined('TYPO3_MODE') || die('Access denied.');


// Add Content Elements
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig('<INCLUDE_TYPOSCRIPT: source="FILE:EXT:wem_headless_slider/Configuration/TsConfig/Slider.tsconfig">');

/***************
 * Register Icons
 */
$iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);
$icons = [
    'slider',
    'slider-item',
];
foreach ($icons as $icon) {
    $iconRegistry->registerIcon(
        'content-wemheadlessslider-' . $icon,
        \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
        ['source' => 'EXT:wem_headless_slider/Resources/Public/Icons/' . $icon . '.svg']
    );
}
