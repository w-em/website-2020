<?php

/*
 * This file is part of the package wem/wem-theme.
 *
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

defined('TYPO3_MODE') || die();

$GLOBALS['TCA']['tt_content']['columns']['tx_wemheadlessslider_slider_item'] = [
    'label' => 'LLL:EXT:wem_headless_slider/Resources/Private/Language/Backend.xlf:slider.item',
    'config' => [
        'type' => 'inline',
        'foreign_table' => 'tx_wemheadlessslider_slider_item',
        'foreign_field' => 'tt_content',
        'appearance' => [
            'useSortable' => true,
            'showSynchronizationLink' => true,
            'showAllLocalizationLink' => true,
            'showPossibleLocalizationRecords' => true,
            'showRemovedLocalizationRecords' => false,
            'expandSingle' => true,
            'enabledControls' => [
                'localize' => true,
            ]
        ],
        'behaviour' => [
            'mode' => 'select',
        ]
    ]
];

