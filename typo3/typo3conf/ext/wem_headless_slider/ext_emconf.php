<?php

$EM_CONF[$_EXTKEY] = array(
    'title' => 'Headless Slider',
    'description' => 'Headless Slider json output for EXT:headless',
    'category' => 'fe',
    'author' => 'Stefan Jaeger',
    'author_email' => 'sj@w-em.com',
    'author_company' => 'w-em GmbH',
    'state' => 'beta',
    'clearCacheOnLoad' => 0,
    'version' => '1.0.0',
    'constraints' =>
        array(
            'depends' =>
                array(
                    'typo3' => '10.0.0-10.4.99',
                    'headless' => '2.0.0-2.99.99',
                    'gridelements' => '10.0.0-10.99.99',
                ),
            'conflicts' =>
                array(),
            'suggests' =>
                array(),
        ),
    'autoload' =>
        array(
            'psr-4' =>
                array(
                    'Wem\\HeadlessSlider\\' => 'Classes',
                ),
        ),
    'autoload-dev' =>
        array(
            'psr-4' =>
                array(
                    'Wem\\HeadlessSlider\\Tests\\' => 'Tests',
                ),
        ),
    'uploadfolder' => false,
    'clearcacheonload' => false,
);

