module.exports = {
  mode: 'universal',
  ssr: false,
  telemetry: false,
  /*
   ** Headers of the page
   */

  head: {
    title: 'SCHLIESSKE MARKENAGENTUR',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'description',
        name: 'description',
        content: process.env.npm_package_description || ''
      }
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }]
  },
  /*
   ** Customize the progress-bar color
   */
  loading: { color: '#d60b52' },
  /*
   ** Global CSS
   */
  css: ['~/assets/scss/custom.scss'],
  /*
   ** Plugins to load before mounting the App
   */
  plugins: [
    '~/plugins/components',
    '~/plugins/layouts',
    '~/plugins/vue-transitions',
    '~/plugins/vue-scroll-lock',
    '~/plugins/vue-awesome-swiper'
  ],
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: [
    // Doc: https://github.com/nuxt-community/eslint-module
    '@nuxtjs/eslint-module',
    // Doc: https://github.com/nuxt-community/stylelint-module
    '@nuxtjs/stylelint-module'
  ],
  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc: https://bootstrap-vue.js.org
    'bootstrap-vue/nuxt',
    // Doc: https://github.com/mercs600/nuxt-typo3-skin
    // 'nuxt-typo3-skin',
    // 'nuxt-typo3-headless-news',
    // Doc: https://github.com/TYPO3-Initiatives/nuxt-typo3
    'nuxt-typo3',
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    '@nuxtjs/pwa',
    // Doc: https://github.com/nuxt-community/dotenv-module
    '@nuxtjs/dotenv',
    '@nuxtjs/svg'
  ],
  /*
   ** TYPO3 module configuration
   ** https://github.com/TYPO3-Initiatives/nuxt-typo3
   */
  typo3: {
    baseURL: process.env.NUXT_HOST,
    api: {
      baseURL: 'https://dev.schliesske.de.w-em.com'
    }
  },
  /*
   ** Axios module configuration
   ** See https://axios.nuxtjs.org/options
   */
  axios: {},
  /*
   ** Build configuration
   */
  build: {
    /*
     ** You can extend webpack config here
     */
    extend(config, ctx) {},
    transpile: ['vue-scroll-lock', 'vue2-transitions', 'swiper']
  },
  rules: [
    {
      test: /\.s[ac]ss$/i,
      use: ['style-loader', 'css-loader', 'sass-loader']
    }
  ],
  /* page Transitions */
  transition: {
    name: 'page',
    mode: 'out-in'
  }
}
