import Vue from 'vue'
import { registerBackendLayouts } from '~typo3/plugins/layouts'
import BeDefault from '~/layouts/backend/BeDefault'
const layouts = {
  BeDefault
}

export default ({ app }) => {
  Vue.use(registerBackendLayouts, {
    context: app,
    layouts
  })
}
