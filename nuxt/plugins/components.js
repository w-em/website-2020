import Vue from 'vue'
import CeFrame from '~/components/content/elements/CeFrame'
import CoreImg from '~/components/content/core/CoreImage'

import CeBullets from '~/components/content/elements/CeBullets'
import CeDiv from '~/components/content/elements/CeDiv'
import CeFormFormframework from '~/components/content/elements/CeForm_formframework'
import CeHeader from '~/components/content/elements/CeHeader'
import CeHtml from '~/components/content/elements/CeHtml'
import CeImage from '~/components/content/elements/CeImage'
import CeMenuAbstract from '~/components/content/elements/CeMenuAbstract'
import CeMenuPages from '~/components/content/elements/CeMenuPages'
import CeMenuSection from '~/components/content/elements/CeMenuSection'
import CeMenuCardList from '~/components/content/elements/CeMenuCardList'
import CeMenuSitemap from '~/components/content/elements/CeMenuSitemap'
import CeShortcut from '~/components/content/elements/CeShortcut'

import CeTable from '~/components/content/elements/CeTable'
import CeText from '~/components/content/elements/CeText'
import CeTextmedia from '~/components/content/elements/CeTextmedia'
import CeTextpic from '~/components/content/elements/CeTextpic'
import CeUploads from '~/components/content/elements/CeUploads'
import CeMediaFile from '~/components/content/elements/media/File'
import MediaImage from '~/components/content/elements/media/type/Image'
import CeStructuredContent from '~/components/content/elements/CeStructuredContent'
import CeReferenceSlider from '~/components/content/elements/CeReferenceSlider'
import CeContact from '~/components/content/elements/CeContact'

// layout
import FooterMain from '~/components/layout/FooterMain'
import HeaderMain from '~/components/layout/HeaderMain'

// ui
import Logo from '~/components/UI/Logo'
import NavigationMain from '~/components/UI/NavigationMain'
import NavigationMobile from '~/components/UI/NavigationMobile'
import ResponsiveHelper from '~/components/UI/ResponsiveHelper'

// news
import CeNewsPi1 from '~/components/content/elements/CeNews_pi1'

const components = {
  CoreImg,

  CeFrame,
  CeBullets,
  CeDiv,
  CeFormFormframework,
  CeHeader,
  CeHtml,
  CeImage,
  CeMenuAbstract,
  CeMenuPages,
  CeMenuCardList,
  CeMenuSitemap,
  CeMenuSection,
  CeShortcut,
  CeTable,
  CeText,
  CeTextmedia,
  CeTextpic,
  CeUploads,
  CeMediaFile,
  MediaImage,
  CeStructuredContent,
  FooterMain,
  HeaderMain,
  Logo,
  NavigationMain,
  NavigationMobile,
  ResponsiveHelper,

  CeNewsPi1,
  CeReferenceSlider,
  CeContact
}

export default ({ app }) => {
  Object.keys(components).forEach((key) => {
    Vue.component(key, components[key])
  })
}
